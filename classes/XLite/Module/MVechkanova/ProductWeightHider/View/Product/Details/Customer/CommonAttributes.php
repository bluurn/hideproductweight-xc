<?php
// vim: set ts=4 sw=4 sts=4 et:

namespace XLite\Module\MVechkanova\ProductWeightHider\View\Product\Details\Customer;

/**
 * Product attributes
 */
class CommonAttributes extends \XLite\View\Product\Details\Customer\CommonAttributes implements \XLite\Base\IDecorator
{
    /**
     * Check if weight is hidden
     *
     * @return boolean
     */
    public function isWeightHidden()
    {
      return (bool) \XLite\Core\Config::getInstance()->MVechkanova->ProductWeightHider->pwh_hide_product;
    }
}
